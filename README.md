# webscan2pdf

## Resumen

Proyecto experimental que genera 1 pdf  por cada registro de archivo al cual se le incorpora el código de barra que se lee desde una webcam

## Requisitos

pyzbar cv2

## Pequeña descripción

El programa lee y recorre el archivo CSV, en el cual tiene definido un listado de personas que pertenecen a determinadas instituciones.
Inicia la webcam de la computadora con la cual se lee el codigo de barras del dispositivo que se le asigna a esta persona, cuando el sistema lee el codigo en la pantalla se despliegan los datos de la institución, persona y número de serie para corroborar los datos.

<img src="pantalla.png" alt="Pantalla" title="Pantalla para corroborar datos" width="300" height="300" />

Si los datos son correctos se debe presionar la tecla "s" para continuar con el próximo registro y genera un PDF (con base del diseño definido en archivo SVG) por persona, el cual sirve de certificado. Tambien genera otro archivo csv con los datos de la institucion, persona y dispositivo.





