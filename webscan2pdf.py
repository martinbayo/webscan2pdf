# encoding=utf8
from pyzbar import pyzbar
import argparse
import cv2
import io

# Importamos libs
import os, sys
import smtplib
import csv
import subprocess
import time

# Lista de escuelas y estudiantes por ; y se definen campos
listado_inicial_file = 'listado_inicial.csv'
persona_dni = 4
persona_nombre = 3
institucion_localidad = 2
institucion_nombre = 1
institucion_cod = 0


ap = argparse.ArgumentParser()
#ap.add_argument("-i", "--image", required=True, help="path to input image")
args = vars(ap.parse_args())

cap = cv2.VideoCapture(0)
codigobarra = 0
with open(listado_inicial_file, 'rt') as csvfile:
    lista_original = csv.reader(csvfile, delimiter=';', quotechar='|')
    for registro in lista_original:
        cod_institucion = registro[institucion_cod]
        nombre_institucion = registro[institucion_nombre]
        localidad_institucion = registro[institucion_localidad]
        dni_persona = registro[persona_dni]
        nombre_persona = registro[persona_nombre]
        if not cap.isOpened():
            print("Error")
        while cap.isOpened():
            ret,frame = cap.read()
            if ret:
               barcodes = pyzbar.decode(frame)
               for barcode in barcodes :
                    (x, y, w, h) = barcode.rect
                    cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 0, 255), 2)
                    image = cv2.putText(frame, nombre_institucion, (x, y -50), cv2.FONT_HERSHEY_SIMPLEX,1, (255, 0, 0), 5)
                    image = cv2.putText(frame, nombre_persona, (x, y -100), cv2.FONT_HERSHEY_SIMPLEX,1, (255, 0, 0), 5)
                    barcodeData = barcode.data.decode("utf-8")
                    barcodeType = barcode.type
                    text = "{} ({})".format(barcodeData, barcodeType)
                    cv2.putText(frame, text, (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
                    print("[INFO] Found {} barcode: {}".format(barcodeType, barcodeData))
                    codigobarra = barcodeData
               cv2.imshow('Frame', frame)
               #time.sleep(2)

#                        listadofinal_grabar.writerow('\r\n')
               if cv2.waitKey(25) & 0xFF == ord('s'):
                    with open('listado_final.csv',mode='a') as listadofinal:
                         listadofinal_grabar = csv.writer(listadofinal, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                         listadofinal_grabar.writerow([nombre_institucion,cod_institucion,localidad_institucion,dni_persona,nombre_persona,codigobarra])
               #Mejorar se cambian campos de Nombre y DNI del certigicado 
                    open('temp.svg', 'w').write(open('certificado.svg').read().replace('{{instnombre}}',nombre_institucion))
                    open('temp1.svg', 'w').write(open('temp.svg').read().replace('{{localidad}}',localidad_institucion))
                    open('temp2.svg', 'w').write(open('temp1.svg').read().replace('{{numserie}}',codigobarra))
                    open('temp3.svg', 'w').write(open('temp2.svg').read().replace('{{nombrepersona}}',nombre_persona))
                    open('temp4.svg', 'w').write(open('temp3.svg').read().replace('{{DNI}}',dni_persona))

                   #se exporta a PDF  y se guardan para .... ?
                    cmd = ['inkscape', '--export-pdf='+dni_persona+'.pdf', 'temp4.svg']
                    subprocess.check_call(cmd)

                    break
        #else:
        #    break
    cap.release()
    cv2.destroyAllWindows()

